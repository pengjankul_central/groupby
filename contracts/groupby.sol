pragma solidity ^0.8.0;


import "hardhat/console.sol";

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/access/AccessControl.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract group_by is Ownable {

    
    ERC20 private ERC20interface;
    address public stable_address = 0xD8c2D743B9c543ff3f770dd53648eEd9c631Faf2;


    string private pid;
    string private pname;


    uint256 private target_qty;
    uint256 private minimum_qty;
    uint256 private current_qty; 
    uint256 private deposit_balace;
    uint256 private unit_price; 
   

    mapping(address => uint256) balaceOf;
    
    constructor(string memory _pid,string memory _pname,uint256 _target_qty,uint256 _unit_price) {
        pid = _pid;
        pname = _pname;
        target_qty = _target_qty;
        unit_price = _unit_price;
        ERC20interface = ERC20(stable_address);
    }

    function getProductID() public view returns (string memory) {
        return pid;
    }
    function getProductName() public view returns (string memory) {
        return pname;
    }

    function getTargetQTY() public view returns (uint256) {
        return target_qty;
    }

    function getUnitPrice() public view returns (uint256){
        return unit_price;
    }

    function calculate_price_deposit (uint _qty) public view returns (uint256){
        return _qty * unit_price;
    }

    function isGoalAchieved() public view returns (bool){
        if ( (target_qty == current_qty ) && (ERC20interface.balanceOf(address(this))== calculate_price_deposit(target_qty) ) ){
            return true;
        }
        return false;
    }

    function emergencyWithdraw()public payable{

    }

    function deposit_fund(uint256 _value,uint _qty) public payable {
        require(_value == calculate_price_deposit(_qty) );
        address from = msg.sender;
        address to = address(this);
        ERC20interface.transferFrom(from, to, _value);
        current_qty += _qty;
    
    }

    function getBalance() public view returns (uint256){
        return ERC20interface.balanceOf(address(this));
    }

    function approveSpendToken(uint _amount) public returns(bool){
        return ERC20interface.approve(address(this), _amount); 
    }

    function cancelOrder (address _to) public payable   {
            _to = msg.sender;
            uint balance = ERC20interface.balanceOf(address(this));
            ERC20interface.transferFrom(address(this), _to, balance);
    }

     
/*
    function refunds() public onlyOwner {

    }
    function release() public onlyOwner{

    }
    function cancel() public {

    }

    function getBalance() public  returns (uint) {
        
    }
 */

}